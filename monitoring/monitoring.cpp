#include "ros/ros.h"
#include <ros/time.h>
#include <stdio.h>
#include <cstdlib>
#include <actionlib/client/simple_action_client.h>
#include <actionlib/client/terminal_state.h>
#include <std_msgs/UInt8.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

//////////////////////////////////
#include <opencv2/core/core.hpp>
//////////////////////////////////

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include <math.h>
#include <unistd.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/QuaternionStamped.h>
#include <sensor_msgs/NavSatFix.h>
//#include <djiControl.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <curl/curl.h>
#include <curl/easy.h>
#include "src/GeoCoordConv.h"
#include <string>
#include "pix_gtc/Srv_key.h"
#include "pix_gtc/monitor_key.h"
#include <pix_gtc/Waypoint.h>
#include <pix_gtc/WaypointInform.h>
#include <pix_gtc/WaypointList.h>
#include <tf/tf.h>

using namespace std;
using namespace cv;

float gps_[4];
cv::Point2d wayPointConv;
cv::Point2d wayPointWGS84;

pix_gtc::WaypointList waypointList;
double rR, rP, rY;
double rRoll, rPitch, rYaw;

// CODE_ABOUT_GOOGLEMAP_START

#define GPSMAP_RESOL 0.12 // 20 size
#define GPSIMG_SIZE 640
#define GPS_DEFAULT_LON 127.362826
#define GPS_DEFAULT_LAT 36.365959

bool GPSValid = false;
bool isLoadMap = false;

cv::Point2d mapConvXY;
cv::Point2d currConvXY;
cv::Point2d gpsRef(0., 0.);
cv::Point2d wgs84(GPS_DEFAULT_LON, GPS_DEFAULT_LAT);

cv::Mat Gpsimg;
std::list<cv::Point2d> fFootprintPts;
std::list<cv::Point2d> fFootprintPtsApril;
int MapZoomLevel = 19;
double MeterperPixel = GPSMAP_RESOL * pow(2., 20. - static_cast<float>(MapZoomLevel));

cv::Point2d Cvt_Map2ConvXY(cv::Point pixel_xy, double img_MeterperPix, cv::Point2d origin_conv)
{
    cv::Point2d convXY;

    convXY.x = (double)(((double)pixel_xy.x - GPSIMG_SIZE / 2) * img_MeterperPix + origin_conv.x);
    convXY.y = (double)((-(double)pixel_xy.y + GPSIMG_SIZE / 2) * img_MeterperPix + origin_conv.y);

    return convXY;
}

cv::Point Cvt_ConvXY2Map(cv::Point2d convXY, double img_MeterperPix, cv::Point2d origin_conv)
{
    cv::Point pixel_xy;

    pixel_xy.x = (int)((double)(convXY.x - origin_conv.x) / img_MeterperPix + GPSIMG_SIZE / 2);
    pixel_xy.y = (int)((double)(convXY.y - origin_conv.y) / img_MeterperPix - GPSIMG_SIZE / 2);

    pixel_xy.y *= -1;
    return pixel_xy;
}

cv::Point2d Cvt_Conv2WGS84(cv::Point2d GPS_convXY)
{
    CGeoCoordConv CoordConv;
    CoordConv.SetSrcType(kBessel1984, kTmMid);
    CoordConv.SetDstType(kWgs84, kGeographic);

    double lat, lon;
    CoordConv.Conv(GPS_convXY.x, GPS_convXY.y, lon, lat);
    cv::Point2d result_lonlat = cv::Point2d(lon, lat);
    return result_lonlat;
}

cv::Point2d Cvt_WGS842Conv(cv::Point2d GPS_lonlat)
{
    CGeoCoordConv CoordConv;
    CoordConv.SetSrcType(kWgs84, kGeographic);
    CoordConv.SetDstType(kBessel1984, kTmMid);

    double convX, convY;
    CoordConv.Conv(GPS_lonlat.x, GPS_lonlat.y, convX, convY);
    cv::Point2d result_convXY = cv::Point2d(convX, convY);
    return result_convXY;
}

size_t write_data(char *ptr, size_t size, size_t nmemb, void *userdata)
{
    std::ostringstream *stream = (std::ostringstream*)userdata;
    size_t count = size * nmemb;
    stream->write(ptr, count);
    return count;
}

cv::Mat GetimgFromURL(std::string strm)
{
    CURL *curl;
    CURLcode res;
    std::ostringstream stream;
    curl = curl_easy_init();
    curl_easy_setopt(curl, CURLOPT_URL, strm.c_str()); //the img url
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data); // pass the writefunction
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &stream); // pass the stream ptr when the writefunction is called
    res = curl_easy_perform(curl); // start curl
    std::string output = stream.str(); // convert the stream into a string
    curl_easy_cleanup(curl); // cleanup
    std::vector<char> data = std::vector<char>(output.begin(), output.end()); //convert string into a vector
    cv::Mat data_mat = cv::Mat(data); // create the cv::Mat datatype from the vector
    cv::Mat image = cv::imdecode(data_mat, 1); //read an image from memory buffer

    return image;
}

void MapUpdate(cv::Point2d current_ConvXY)
{
    std::stringstream strm;

    strm << "http://maps.googleapis.com/maps/api/staticmap?center=";
    cv::Point2d wgs84XY = Cvt_Conv2WGS84(current_ConvXY);

    strm.precision(8);
    strm << wgs84XY.y << ",";
    strm.precision(9);
    strm << wgs84XY.x;
    strm << "&zoom=";
    strm << MapZoomLevel;
    strm << "&size=640x640";
    strm << "&maptype=satellite";
    strm << "&sensor=false";

    Gpsimg = GetimgFromURL(strm.str());

    isLoadMap = true;
    mapConvXY = current_ConvXY;
}

void DrawPosImg()
{
    cv::Mat _Map = Gpsimg.clone();
    int face[] = { cv::FONT_HERSHEY_SIMPLEX, cv::FONT_HERSHEY_PLAIN, cv::FONT_HERSHEY_DUPLEX, cv::FONT_HERSHEY_COMPLEX, cv::FONT_HERSHEY_TRIPLEX, cv::FONT_HERSHEY_COMPLEX_SMALL, cv::FONT_HERSHEY_SCRIPT_SIMPLEX,
                   cv::FONT_HERSHEY_SCRIPT_COMPLEX, cv::FONT_ITALIC };

    // old path drawing
    std::list<cv::Point2d>::iterator i = fFootprintPts.begin();
    for (; i != fFootprintPts.end(); i++)
        cv::circle(_Map, Cvt_ConvXY2Map(*i, MeterperPixel, mapConvXY), 1, cvScalar(0, 0, 255), 1);

    cv::Point preWayPointMap;
    // Waypoints drawing
    std::vector<pix_gtc::WaypointInform>::iterator k = waypointList.WaypointList.begin();
    for (; k != waypointList.WaypointList.end(); k++)
    {
        cv::Point2d wayPointConv = Cvt_WGS842Conv(cv::Point2d(k->longitude, k->latitude));
        cv::Point wayPointMap = Cvt_ConvXY2Map(wayPointConv + gpsRef, MeterperPixel, mapConvXY);
        cv::circle(_Map, wayPointMap, 2, cvScalar(255, 255, 0), 2);
        if (k != waypointList.WaypointList.begin()) cv::line(_Map, preWayPointMap, wayPointMap, cvScalar(255, 255, 0), 1);
        preWayPointMap = wayPointMap;
    }

    // Crt Position
    cv::Point robotPoint = Cvt_ConvXY2Map(currConvXY + gpsRef, MeterperPixel, mapConvXY);
    cv::circle(_Map, robotPoint, 5, cvScalar(0, 255, 0), 2);
    int dx = (int)(sin(rY)*15);
    int dy = (int)(cos(rY)*15);
    cv::line(_Map, robotPoint + cv::Point(dx, -dy), robotPoint, cvScalar(0, 255, 0), 2);

    if (waypointList.WaypointList.size() > 0)
    {
        pix_gtc::WaypointInform waypoint = waypointList.WaypointList[0];
        cv::Point2d wayPointConv = Cvt_WGS842Conv(cv::Point2d(waypoint.longitude, waypoint.latitude)) + gpsRef;
        cv::Point wayPointMap = Cvt_ConvXY2Map(wayPointConv, MeterperPixel, mapConvXY);
        cv::line(_Map, robotPoint, wayPointMap, cvScalar(0, 255, 255), 1);
    }


    cv::imshow("GPS", _Map);

    cvWaitKey(1);
}

// CODE_ABOUT_GOOGLEMAP_END






ros::Publisher wplist_publisher;



void timer_callback(const ros::TimerEvent&)
{
    //ROS_INFO("Callback 1 triggered");

    currConvXY = Cvt_WGS842Conv(wgs84);
    fFootprintPts.push_back(currConvXY + gpsRef);

    if (fFootprintPts.size() > 1000)
        fFootprintPts.pop_front();

    // Map Update
    int DistanceDiff = (int)sqrt(pow((mapConvXY.x - currConvXY.x), 2) + pow((mapConvXY.y - currConvXY.y), 2));
    if (isLoadMap && DistanceDiff >= GPSIMG_SIZE / 2 * MeterperPixel)
        MapUpdate(currConvXY);
    //

    // Waypoint check
    if (waypointList.WaypointList.size() > 0)
    {
        pix_gtc::WaypointInform tempWaypoint = waypointList.WaypointList[0];
        cv::Point2d robotpointConv = Cvt_WGS842Conv(cv::Point2d(gps_[0], gps_[1]));
        cv::Point2d waypointConv = Cvt_WGS842Conv(cv::Point2d(tempWaypoint.longitude, tempWaypoint.latitude));
        cv::Point2d diff = waypointConv - robotpointConv;
        double distance = cv::sqrt(diff.x*diff.x + diff.y*diff.y);
        if (distance < 1) waypointList.WaypointList.erase(waypointList.WaypointList.begin());
    }
    //
    wplist_publisher.publish(waypointList);

    if (isLoadMap) DrawPosImg();
}

void CallBackKeyFunc(int event, int x, int y, int flags, void* userdata)
{
    cv::Mat _Map = Gpsimg.clone();

    if (event == cv::EVENT_RBUTTONDOWN)
    {
        cout << "position (" << x << ", " << y << ")" << endl;

        cv::Point2d clickPointConvXY = Cvt_Map2ConvXY(cvPoint(x, y), MeterperPixel, mapConvXY);
        cv::Point2d clickPointWGS84 = Cvt_Conv2WGS84(clickPointConvXY);
        cout.precision(10);
        cout << "Lon : " << clickPointWGS84.x << " / Lat : " << clickPointWGS84.y << endl;

        gpsRef = clickPointConvXY - currConvXY;

        std::ofstream refout("../../../src/pix_GTC/gpsref", ios::out);
        refout << "x" << "\t" << gpsRef.x << endl
               << "y" << "\t" << gpsRef.y;
        refout.close();
    }
    else if (event == cv::EVENT_LBUTTONDOWN)
    {
        cv::Point2d clickPointConvXY = Cvt_Map2ConvXY(cvPoint(x, y), MeterperPixel, mapConvXY) - gpsRef;
        cv::Point2d clickPointWGS84 = Cvt_Conv2WGS84(clickPointConvXY);
        cout.precision(10);
        //cout << "Lon : " << clickPointWGS84.x << " / Lat : " << clickPointWGS84.y << endl;

        wayPointConv = clickPointConvXY;
        wayPointWGS84 = clickPointWGS84;



//        samsung_gtc::Waypoint srv;
//        srv.request.longitude = wayPointWGS84.x;
//        srv.request.latitude = wayPointWGS84.y;



//        samsung_gtc::WaypointInform waypoint;
//        waypoint.longitude = wayPointWGS84.x;
//        waypoint.latitude = wayPointWGS84.y;
//        waypointList.WaypointList.push_back(waypoint);


        pix_gtc::WaypointInform waypoint;
        waypoint.longitude = wayPointWGS84.x;
        waypoint.latitude = wayPointWGS84.y;
        waypoint.altitude = gps_[2];
        waypointList.WaypointList.push_back(waypoint);
    }

    else if(event == cv::EVENT_MBUTTONDOWN){

        waypointList.WaypointList.pop_back();

    }

    for(int i=0; i<waypointList.WaypointList.size(); i++){
      printf("%lf %lf %ld\n", waypointList.WaypointList[i].longitude, waypointList.WaypointList[i].latitude, waypointList.WaypointList.size());
    }
//    ROS_ERROR("!!!!");
}

bool wp_insert(pix_gtc::monitor_key::Request &req, pix_gtc::monitor_key::Response &res){
    int operate_code = req.key;

    if(operate_code == 'g'){
      res.wpList = waypointList;
      res.res = true;
      return true;
    }
    else if(operate_code == 's'){
      waypointList.WaypointList.clear();
    }
    else{
      res.res = false;
      return false;
    }

}


void rpy_callback(const nav_msgs::Odometry::ConstPtr &msg)
{
    double orientation[4] = {0};
    orientation[0] = msg->pose.pose.orientation.x;
    orientation[1] = msg->pose.pose.orientation.y;
    orientation[2] = msg->pose.pose.orientation.z;
    orientation[3] = msg->pose.pose.orientation.w;

    tf::Quaternion q(orientation[0], orientation[1], orientation[2], orientation[3]);
    tf::Matrix3x3 m(q);
    m.getRPY(rR, rP, rY);
    rRoll = rR * 180./CV_PI;    rPitch = rP*180./CV_PI;    rYaw = rY*180./CV_PI;
    cout << gpsRef.x << gpsRef.y << endl;
    //ROS_INFO("roll: %.5f, pitch: %.5f, yaw: %.5f", m_rRoll, m_rPitch, m_rYaw);
}


void gps_callback(sensor_msgs::NavSatFix msg)
{
    gps_[0] = msg.longitude;
    gps_[1] = msg.latitude;
    gps_[2] = msg.altitude;

    /*
    cout << "Lon:" << gps_[0] << ", "
         << "Lat:" << gps_[1] << ", "
         << "Alt:" << gps_[2] << endl;
    */

    wgs84.x = static_cast<double>(gps_[0]);
    wgs84.y = static_cast<double>(gps_[1]);

    GPSValid = true;
}

int main(int argc, char **argv)
{
    std::ifstream ref("../../../src/pix_GTC/gpsref");
    string temp;
    ref >> temp >> gpsRef.x;
    ref >> temp >> gpsRef.y;
    ref.close();

    cv::namedWindow("GPS", 1);
    cv::setMouseCallback("GPS", CallBackKeyFunc, NULL);

    mapConvXY = Cvt_WGS842Conv(wgs84);
    MapUpdate(mapConvXY);

    ros::init(argc, argv, "monitoring");

    ros::NodeHandle nh;
//    ros::Subscriber sub_gps = nh.subscribe("/dji_sdk/gps_position", 100, gps_callback);
//    ros::Subscriber sub_rpy = nh.subscribe("/dji_sdk/attitude", 1000, rpy_callback);
    ros::Subscriber sub_gps = nh.subscribe("/mavros/global_position/global", 100, gps_callback);
    ros::Subscriber sub_rpy = nh.subscribe("/mavros/global_position/local", 1000, rpy_callback);
    ros::Timer timer1 = nh.createTimer(ros::Duration(0.1), timer_callback);

    ros::ServiceServer monitor_server = nh.advertiseService("monitor_srv", wp_insert);


    wplist_publisher = nh.advertise<pix_gtc::WaypointList>("/monitoring/WaypointList",1);

    ros::spin();

    return 0;
}
