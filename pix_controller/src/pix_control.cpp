#include "pix_control.h"

pix_control::pix_control()
{

  ///////////////////////////////////////////////////
  //////// client
  client_cmdOffBoard = nh.serviceClient<mavros_msgs::SetMode>("/mavros/set_mode");
  client_cmdArming = nh.serviceClient<mavros_msgs::CommandBool>("/mavros/cmd/arming");
  client_cmdTakeoff = nh.serviceClient<mavros_msgs::CommandTOL>("/mavros/cmd/takeoff");
  client_cmdLand = nh.serviceClient<mavros_msgs::CommandTOL>("/mavros/cmd/land");
  client_cmdSetHome = nh.serviceClient<mavros_msgs::CommandHome>("/mavros/cmd/set_home");

  client_cmdParam = nh.serviceClient<mavros_msgs::ParamGet>("/mavros/param/get");

  /////////////////////////////////////////////////////
  /////////// pub
//  pub_cmdCurrentRPY = nh.advertise;
  pub_cmdAttitude = nh.advertise<geometry_msgs::PoseStamped>("/mavros/setpoint_position/local", 10);
//  pub_cmdAttitude = nh.advertise<mavros_msgs::AttitudeTarget>("/mavros/setpoint_raw/attitude", 10);
  pub_cmdGPS = nh.advertise<geometry_msgs::PoseStamped>("/mavros/setpoint_position/local", 10);
  pub_cmdRCin = nh.advertise<std_msgs::Int16MultiArray>("/end_run/main_core/rc",10);


  ////////////////////////////////////////////////////
  ////////////////////////////////////////////////////
  /// ////////////////////////////////////////////////////

  pub_cmdVel = nh.advertise<mavros_msgs::PositionTarget>("/mavros/setpoint_raw/local",10);
  sub_pix_target_local = nh.subscribe<mavros_msgs::PositionTarget>("/mavros/setpoint_raw/target_local",1, &pix_control::callback_Target,this);


  ////////////////////////////////////////////////////
  /// ////////////////////////////////////////////////////
  /// ////////////////////////////////////////////////////
  ///////////////////////////////////////////////
  ////////// sub
  sub_pix_IMU = nh.subscribe<sensor_msgs::Imu>("/mavros/imu/data",1, &pix_control::callback_IMU,this);
  sub_pix_current_target_attitude = nh.subscribe<mavros_msgs::AttitudeTarget>("/mavros/setpoint_raw/target_attitude",10, &pix_control::callback_targetAttitude,this);
  sub_pix_state = nh.subscribe<mavros_msgs::State>("/mavros/state", 1, &pix_control::callback_state, this);
  sub_pix_poseGloabl = nh.subscribe<sensor_msgs::NavSatFix>("/mavros/global_position/global",1, &pix_control::callback_GPS,this);
  sub_pix_poseLocal = nh.subscribe<nav_msgs::Odometry>("/mavros/global_position/local", 1, &pix_control::callback_local, this);
  sub_pix_homePosition = nh.subscribe<mavros_msgs::HomePosition>("/mavros/home_position/home",1, &pix_control::callback_homePosition,this);
  sub_pix_rc = nh.subscribe<mavros_msgs::RCIn>("/mavros/rc/in",1, &pix_control::callback_rc_in,this);
//  sub_pix_rc_out = nh.subscribe

  connectItr = 0;
  yawInRad = 0;

  flagOffboard = false;
  pauseItr = 0;
  flagState = 0;
  flagWp = -1;
  idxWp = 0;
  sizeWp = 0;

  last_request = ros::Time::now();
  offb_set_mode.request.custom_mode = "OFFBOARD";



  pix_flag = 255;
  pix_desired.coordinate_frame = 8;
  pix_desired.type_mask = 1991;

}

pix_control::~pix_control()
{

}


void pix_control::spinOnce()
{
  ros::Rate rate(20.0);

  while(ros::ok() && !pix_state.connected && connectItr != 10){
      ros::spinOnce();
      rate.sleep();
      ROS_INFO("connect..loading...%d",connectItr);
      connectItr++;
  }


  if(pix_state_old.mode == "OFFBOARD" && pix_state.mode != "OFFBOARD"){
    flagOffboard = false;
    flagState = 2;
    ROS_ERROR("OFF byebye");
  }

  if(flagOffboard){
    if(pix_state.mode != "OFFBOARD" && (ros::Time::now() - last_request > ros::Duration(1.0))){

      if(client_cmdOffBoard.call(offb_set_mode) && offb_set_mode.response.mode_sent){
        ROS_INFO("Offboard enabled");
      }
      last_request = ros::Time::now();
    }
  }

  double dx = std::abs(pix_set.pose.pose.position.x - pix_poseLocal.pose.pose.position.x);
  double dy = std::abs(pix_set.pose.pose.position.y - pix_poseLocal.pose.pose.position.y);
  double dz = std::abs(pix_set.pose.pose.position.z - pix_poseLocal.pose.pose.position.z);

//  geometry_msgs::Quaternion q_cur;

  geometry_msgs::Quaternion q = pix_poseLocal.pose.pose.orientation;
  geometry_msgs::Quaternion q_set = pix_set.pose.pose.orientation;
  double yaw = Quat2Angle(q).z;
  double yaw_set = Quat2Angle(q_set).z;
  double dtheta = std::abs(yaw - yaw_set);

  while((ros::Time::now() - last_request > ros::Duration(1.0))){
//    printf("state : %d \n", flagState);
    printf("flag : %d // dx : %lf // dy : %lf // dz : %lf // dtheta : %lf \n", pix_flag, dx, dy, dz, dtheta);
    last_request = ros::Time::now();
  }



  ///////////////////////////////////////////////
  if(flagState > 3){
//    pub_cmdAttitude.publish(pix_targetLocal);
    pub_cmdVel.publish(pix_desired);

    if(pix_flag == 0){ //take
      if(dz > NUM){
        pix_desired.velocity.x = 0;
        pix_desired.velocity.y = 0;
        pix_desired.velocity.z = 0;
        pix_desired.yaw_rate = 0;
        pix_flag = 255;
      }
    }
    else if(pix_flag == 1){ // up
      if(dz > NUM){
        pix_desired.velocity.x = 0;
        pix_desired.velocity.y = 0;
        pix_desired.velocity.z = 0;
        pix_desired.yaw_rate = 0;
        pix_flag = 255;
      }
    }
    else if(pix_flag == 2){ // down
      if(dz > NUM){
        pix_desired.velocity.x = 0;
        pix_desired.velocity.y = 0;
        pix_desired.velocity.z = 0;
        pix_desired.yaw_rate = 0;
        pix_flag = 255;
      }
    }
    else if(pix_flag == 3){ // +x
      if(dx > NUM){
        pix_desired.velocity.x = 0;
        pix_desired.velocity.y = 0;
        pix_desired.velocity.z = 0;
        pix_desired.yaw_rate = 0;
        pix_flag = 255;
      }
    }
    else if(pix_flag == 4){//-x
      if(dx > NUM){
        pix_desired.velocity.x = 0;
        pix_desired.velocity.y = 0;
        pix_desired.velocity.z = 0;
        pix_desired.yaw_rate = 0;
        pix_flag = 255;
      }
    }
    else if(pix_flag == 5){//+y
      if(dy > NUM){
        pix_desired.velocity.x = 0;
        pix_desired.velocity.y = 0;
        pix_desired.velocity.z = 0;
        pix_desired.yaw_rate = 0;
        pix_flag = 255;
      }
    }
    else if(pix_flag == 6){//-y
      if(dy > NUM){
        pix_desired.velocity.x = 0;
        pix_desired.velocity.y = 0;
        pix_desired.velocity.z = 0;
        pix_desired.yaw_rate = 0;
        pix_flag = 255;
      }
    }
    else if(pix_flag == 7){//landing
      if(dz < 0.1){
        pix_desired.velocity.x = 0;
        pix_desired.velocity.y = 0;
        pix_desired.velocity.z = 0;
        pix_desired.yaw_rate = 0;
        pix_flag = 255;
      }
    }
    else if(pix_flag == 8){//+rot
      if(dtheta > DEG){
        pix_desired.velocity.x = 0;
        pix_desired.velocity.y = 0;
        pix_desired.velocity.z = 0;
        pix_desired.yaw_rate = 0;
        pix_flag = 255;
      }
    }
    else if(pix_flag == 9){//-rot
      if(dtheta > DEG){
        pix_desired.velocity.x = 0;
        pix_desired.velocity.y = 0;
        pix_desired.velocity.z = 0;
        pix_desired.yaw_rate = 0;
        pix_flag = 255;
      }
    }
    else{
      pix_desired.velocity.x = 0;
      pix_desired.velocity.y = 0;
      pix_desired.velocity.z = 0;
      pix_desired.yaw_rate = 0;
      pix_flag = 255;
    }


  }
  //////////////////////////////////////////////






  // Wp mission
  if((flagState == 5) && (idxWp < sizeWp)){
    if(flagWp == -1){
      double yaw_desired, yaw_cur, yaw_delta;
      yaw_desired = Quat2Angle(pix_targetLocal.pose.orientation).z;
      yaw_cur = Quat2Angle(pix_poseLocal.pose.pose.orientation).z;
      yaw_delta = yaw_desired - yaw_cur;
      if(std::abs(yaw_delta * 180 / M_PI) < 0.5){
        flagWp = 0;
        cmd_wp();
      }
    }
    // going Wp
    else if(flagWp == 0){
      double delta_x = pix_poseLocal.pose.pose.position.x - pix_targetLocal.pose.position.x;
      double delta_y = pix_poseLocal.pose.pose.position.y - pix_targetLocal.pose.position.y;
      if((std::abs(delta_x) < 0.5) && (std::abs(delta_y) < 0.5)){
        idxWp++;
        flagWp = 1;

        if(idxWp == sizeWp){
          ROS_INFO("WP mission clear");
          idxWp = 0;
          sizeWp = 0;
          flagWp = -1;
          flagState = 4;
        }
        else{
          printf("WP %d is finished // If you wanna go next WP, Using the key 'g'\n", idxWp);
        }
      }
    }
    // going -> arrive Wp
    else if(flagWp == 1){
      pix_targetLocal.pose.position.x = pix_poseLocal.pose.pose.position.x;
      pix_targetLocal.pose.position.y = pix_poseLocal.pose.pose.position.y;
      flagWp = 2;
    }
    // wait  position
    else if(flagWp == 2){

    }
    //
    else if(flagWp == 3){
      if(pauseItr < 100){
        pix_targetLocal.pose.position.x = pix_poseLocal.pose.pose.position.x;
        pix_targetLocal.pose.position.y = pix_poseLocal.pose.pose.position.y;
        printf("pause_target_x : %lf // pause_targt_y : %lf \n", pix_targetLocal.pose.position.x, pix_targetLocal.pose.position.y);
        pauseItr++;
      }
      else{
        pauseItr = 0;
        flagWp = 4;
      }
    }
    else if(flagWp == 4){

    }
    else if(flagWp == 5){
      pix_targetLocal.pose.position.x = pix_wpLocal[idxWp].pose.position.x;
      pix_targetLocal.pose.position.y = pix_wpLocal[idxWp].pose.position.y;
      flagWp = 0;
    }
    else if(flagWp == 6){
      if(pauseItr < 100){
        pix_targetLocal.pose.position.x = pix_poseLocal.pose.pose.position.x;
        pix_targetLocal.pose.position.y = pix_poseLocal.pose.pose.position.y;
        printf("stop_target_x : %lf // stop_targt_y : %lf \n", pix_targetLocal.pose.position.x, pix_targetLocal.pose.position.y);
        pauseItr++;
      }
      else{
        pauseItr = 0;
        flagWp = 7;
      }
    }
    else if(flagWp == 7){
      idxWp = 0;
      sizeWp = 0;
      flagWp = -1;
      flagState = 4;
    }
  }
}

// func.

bool pix_control::check_offboard()
{
  if(pix_state.mode == "OFFBOARD") ROS_INFO("gggg");
  return (pix_state.mode == "OFFBOARD");
}

bool pix_control::check_connected()
{
  return pix_state.connected;
}


geometry_msgs::Vector3 pix_control::Quat2Angle(geometry_msgs::Quaternion quat)
{
  geometry_msgs::Vector3 res;
  tf::Matrix3x3 R_FLU2ENU(tf::Quaternion(quat.x, quat.y, quat.z, quat.w));
  R_FLU2ENU.getRPY(res.x, res.y, res.z);
  return res;
}



tf::Quaternion pix_control::Angle2Quat(geometry_msgs::Vector3 angle)
{
  tf::Matrix3x3 mat;
  mat.setEulerYPR(angle.z, angle.y, angle.x);


  tf::Quaternion tmp;
  mat.getRotation(tmp);
//  tmp = tf::createQuaternionFromYaw(angle.z);

  return tmp;
}


tf::Quaternion pix_control::relativeRotation(geometry_msgs::Vector3 angle)
{
  tf::Quaternion q_rot;
  tf::Quaternion q_new;
  tf::Quaternion q_ori;

  q_ori.setX(pix_poseLocal.pose.pose.orientation.x);
  q_ori.setY(pix_poseLocal.pose.pose.orientation.y);
  q_ori.setZ(pix_poseLocal.pose.pose.orientation.z);
  q_ori.setW(pix_poseLocal.pose.pose.orientation.w);

  q_rot = Angle2Quat(angle);

  q_new = q_rot * q_ori;
  q_new.normalize();

  return q_new;
}

void pix_control::cmd_Rotation(bool flag)
{
  geometry_msgs::Vector3 angle;
  tf::Quaternion q;
  angle.x = 0;
  angle.y = 0;
  angle.z = M_PI * 45 / 180;

  if(flag){
    angle.z = angle.z;
  }
  else{
    angle.z *= -1;
  }

  q = relativeRotation(angle);

  pix_targetLocal.pose.orientation.x = q.getX();
  pix_targetLocal.pose.orientation.y = q.getY();
  pix_targetLocal.pose.orientation.z = q.getZ();
  pix_targetLocal.pose.orientation.w = q.getW();

//  double tmp = Quat2Angle(pix_poseLocal.pose.pose.orientation).z;
//  printf("tmp : %lf \n", tmp * 180 / M_PI);
}


void pix_control::cmd_wp()
{
  if(flagState == 4){
    sizeWp = pix_wpLocal.size();
    idxWp = 0;
    if(sizeWp == 0) ROS_ERROR("Don't have Wp list");
    else flagState = 5;
  }

  if((flagState == 5) && (idxWp < sizeWp) && (flagWp < 1)){
    // rotation move
    if(flagWp == -1){
      geometry_msgs::Quaternion q_cur;
      tf::Quaternion q_desired;
      geometry_msgs::Vector3 rpy_cur, rpy_desired;
      double dx, dy, radi;

//      q_ori.setX(pix_poseLocal.pose.pose.orientation.x);
//      q_ori.setY(pix_poseLocal.pose.pose.orientation.y);
//      q_ori.setZ(pix_poseLocal.pose.pose.orientation.z);
//      q_ori.setW(pix_poseLocal.pose.pose.orientation.w);

      q_cur = pix_poseLocal.pose.pose.orientation;
      rpy_cur = Quat2Angle(q_cur);

      dx = pix_wpLocal[idxWp].pose.position.x - pix_poseLocal.pose.pose.position.x;
      dy = pix_wpLocal[idxWp].pose.position.y - pix_poseLocal.pose.pose.position.y;
      radi = atan2(dx, dy);
//      radi -= M_PI/2;
//      while(radi <= -M_PI) radi += 2 * M_PI;
//      while(radi > M_PI) radi -= 2 * M_PI;

      rpy_desired.x = rpy_cur.x;
      rpy_desired.y = rpy_cur.y;
      rpy_desired.z = radi - rpy_cur.z;
      while(rpy_desired.z <= -M_PI) rpy_desired.z += 2 * M_PI;
      while(rpy_desired.z > M_PI) rpy_desired.z -= 2 * M_PI;

      printf("desired : %lf // cal : %lf // cur : %lf \n", rpy_desired.z*180/M_PI, radi*180/M_PI, rpy_cur.z*180/M_PI);

      q_desired = relativeRotation(rpy_desired);
      pix_targetLocal.pose.orientation.x = q_desired.getX();
      pix_targetLocal.pose.orientation.y = q_desired.getY();
      pix_targetLocal.pose.orientation.z = q_desired.getZ();
      pix_targetLocal.pose.orientation.w = q_desired.getW();
    }
    // translation move
    else{
      pix_targetLocal.pose.position.x = pix_wpLocal[idxWp].pose.position.x;
      pix_targetLocal.pose.position.y = pix_wpLocal[idxWp].pose.position.y;
      printf("WP %d // pt_x : %lf // pt_y : %lf \n", idxWp+1, pix_wpLocal[idxWp].pose.position.x, pix_wpLocal[idxWp].pose.position.y);
    }
  }
}

void pix_control::temp()
{
  double tmp = Quat2Angle(pix_poseLocal.pose.pose.orientation).z;
  printf("tmp : %lf \n", tmp * 180 / M_PI);
}

void pix_control::cmd_OffBoard(bool flag)
{
  flagState = 3;
  flagOffboard = flag;
}

void pix_control::cmd_Arming()
{
  mavros_msgs::CommandBool cmd;
  cmd.request.value = true;
  if(client_cmdArming.call(cmd)){
    flagState = 4;
    ROS_INFO("Arming");
  }
}

void pix_control::cmd_Vel(char flag, char sign)
{

  pix_flag = sign;
  pix_set = pix_poseLocal;


  // 0 ~ 6
  if(flag == 0){ // +x
    pix_desired.velocity.x = VEL;
    pix_desired.velocity.y = 0;
    pix_desired.velocity.z = 0;
    pix_desired.yaw_rate = 0;
  }
  else if(flag == 1){ // -x
    pix_desired.velocity.x = -VEL;
    pix_desired.velocity.y = 0;
    pix_desired.velocity.z = 0;
    pix_desired.yaw_rate = 0;
  }
  else if(flag == 2){ // +y
    pix_desired.velocity.x = 0;
    pix_desired.velocity.y = VEL;
    pix_desired.velocity.z = 0;
    pix_desired.yaw_rate = 0;
  }
  else if(flag == 3){ // -y
    pix_desired.velocity.x = 0;
    pix_desired.velocity.y = -VEL;
    pix_desired.velocity.z = 0;
    pix_desired.yaw_rate = 0;
  }
  else if(flag == 4){ // up
    pix_desired.velocity.x = 0;
    pix_desired.velocity.y = 0;
    pix_desired.velocity.z = VEL;
    pix_desired.yaw_rate = 0;
  }
  else if(flag == 5){ // down
    pix_desired.velocity.x = 0;
    pix_desired.velocity.y = 0;
    pix_desired.velocity.z = -VEL;
    pix_desired.yaw_rate = 0;
  }
  else if(flag == 6){ // +rot
    pix_desired.velocity.x = 0;
    pix_desired.velocity.y = 0;
    pix_desired.velocity.z = 0;
    pix_desired.yaw_rate = ROT;
  }
  else if(flag == 7){ // -rot
    pix_desired.velocity.x = 0;
    pix_desired.velocity.y = 0;
    pix_desired.velocity.z = 0;
    pix_desired.yaw_rate = -ROT;
  }
  else{
    pix_desired.velocity.x = 0;
    pix_desired.velocity.y = 0;
    pix_desired.velocity.z = 0;
    pix_desired.yaw_rate = 0;
  }

}

void pix_control::cmd_TakeOff()
{
  ros::Rate rate(20.0);

//  pix_targetLocal.pose.position.x = 0;
//  pix_targetLocal.pose.position.y = 0;
//  pix_targetLocal.pose.position.z = 2;

  pix_targetLocal.pose.position.x = pix_poseLocal.pose.pose.position.x;
  pix_targetLocal.pose.position.y = pix_poseLocal.pose.pose.position.y;
  pix_targetLocal.pose.position.z = pix_poseLocal.pose.pose.position.z + 1;

//  pix_targetLocal.pose.orientation.x = pix_poseLocal.pose.pose.orientation.x;
//  pix_targetLocal.pose.orientation.y = pix_poseLocal.pose.pose.orientation.y;
//  pix_targetLocal.pose.orientation.z = pix_poseLocal.pose.pose.orientation.z;
//  pix_targetLocal.pose.orientation.w = pix_poseLocal.pose.pose.orientation.w;

  for(int i = 10; ros::ok() && i > 0; --i){
      pub_cmdAttitude.publish(pix_targetLocal);
      ros::spinOnce();
      rate.sleep();
      printf("wait 0 // %d\n", i);

      printf("Yaw :  %lf[rad] // %lf[deg]\n", yawInRad, (yawInRad*180/M_PI));
  }
  ROS_INFO("OK");
}

void pix_control::cmd_Land()
{
  pix_targetLocal.pose.position.x = pix_poseLocal.pose.pose.position.x;
  pix_targetLocal.pose.position.y = pix_poseLocal.pose.pose.position.y;
  pix_targetLocal.pose.position.z = -10;
//  flagState = 3;
}


void pix_control::cmd_Altitude(bool flag)
{
  if(flag){
    pix_targetLocal.pose.position.x = pix_poseLocal.pose.pose.position.x;
    pix_targetLocal.pose.position.y = pix_poseLocal.pose.pose.position.y;
    pix_targetLocal.pose.position.z = pix_poseLocal.pose.pose.position.z + 1;
  }
  else{
    pix_targetLocal.pose.position.x = pix_poseLocal.pose.pose.position.x;
    pix_targetLocal.pose.position.y = pix_poseLocal.pose.pose.position.y;
    pix_targetLocal.pose.position.z = pix_poseLocal.pose.pose.position.z - 1;
  }
}

void pix_control::cmd_posx(bool flag)
{
  if(flag){
    pix_targetLocal.pose.position.x = pix_poseLocal.pose.pose.position.x + 1;
    pix_targetLocal.pose.position.y = pix_poseLocal.pose.pose.position.y;
    pix_targetLocal.pose.position.z = pix_poseLocal.pose.pose.position.z;
  }
  else{
    pix_targetLocal.pose.position.x = pix_poseLocal.pose.pose.position.x - 1;
    pix_targetLocal.pose.position.y = pix_poseLocal.pose.pose.position.y;
    pix_targetLocal.pose.position.z = pix_poseLocal.pose.pose.position.z;
  }
}


void pix_control::cmd_SetHome()
{
  mavros_msgs::CommandHome cmd;
  cmd.request.current_gps = true;
  cmd.request.latitude = pix_poseGlobal.latitude;
  cmd.request.longitude = pix_poseGlobal.longitude;
  cmd.request.altitude = pix_poseGlobal.altitude;

  if(client_cmdSetHome.call(cmd)) {
    flagState = 1;
    ROS_INFO("home set ok");
    sensor_msgs::NavSatFix tmp;
    tmp.latitude = pix_poseGlobal.latitude;
    tmp.longitude = pix_poseGlobal.longitude;
    UTM::UTM(tmp.latitude, tmp.longitude, &pix_homeLocal.pose.position.x, &pix_homeLocal.pose.position.y);
    printf("home position x : %lf // home position y : %lf \n", pix_homeLocal.pose.position.x, pix_homeLocal.pose.position.y);
  }
  else ROS_ERROR("no home set");
}


//geometry_msgs::PoseStamped pix_targetLocal;
//std::vector <geometry_msgs::PoseStamped> pix_wpLocal;
void pix_control::GPS2Local(pix_gtc::WaypointList GPSdata)
{
  wp = GPSdata;
  pix_wpLocal.clear();
  pix_wpLocal.resize(wp.WaypointList.size());


  double local_x, local_y;
  for(int i = 0; i < wp.WaypointList.size(); i++){
    UTM::UTM(wp.WaypointList[i].latitude, wp.WaypointList[i].longitude, &local_x, &local_y);
    pix_wpLocal[i].pose.position.x = local_x;
    pix_wpLocal[i].pose.position.y = local_y;
//    printf("wp_x : %lf // wp_y : %lf \n", pix_wpLocal[i].pose.position.x, pix_wpLocal[i].pose.position.y);

    pix_wpLocal[i].pose.position.x -= pix_homeLocal.pose.position.x;
    pix_wpLocal[i].pose.position.y -= pix_homeLocal.pose.position.y;
    printf("wp_x : %lf // wp_y : %lf \n\n", pix_wpLocal[i].pose.position.x, pix_wpLocal[i].pose.position.y);
  }
}



//////////////////////////////////// call back
void pix_control::callback_IMU(const sensor_msgs::Imu msg)
{
  double roll, pitch, yaw;
  tf::Quaternion q;
  tf::quaternionMsgToTF(msg.orientation, q);
  tf::Matrix3x3(q).getRPY(roll, pitch, yaw);
}

void pix_control::callback_targetAttitude(const mavros_msgs::AttitudeTarget msg)
{
  pix_targetAttitude.thrust = msg.thrust;
}

void pix_control::callback_state(const mavros_msgs::State::ConstPtr &msg)
{
  pix_state_old = pix_state;
  pix_state = *msg;
//  pix_state.
}

void pix_control::callback_GPS(const sensor_msgs::NavSatFix::ConstPtr &msg)
{
  pix_poseGlobal = *msg;
}

void pix_control::callback_local(const nav_msgs::Odometry::ConstPtr &msg)
{
  pix_poseLocal = *msg;
  yawInRad = Quat2Angle(pix_poseLocal.pose.pose.orientation).z;
}


void pix_control::callback_homePosition(const mavros_msgs::HomePosition::ConstPtr &msg)
{
  pix_homePosition = *msg;
}

void pix_control::callback_rc_in(const mavros_msgs::RCIn msg)
{
  rc_in.data.clear();
}

void pix_control::callback_Target(const mavros_msgs::PositionTarget::ConstPtr &msg)
{
  pix_raw = *msg;
}

