#ifndef PIX_CONTROL_H
#define PIX_CONTROL_H

#include "utm.h"
#include "pix_gtc/Srv_key.h"
#include "pix_gtc/monitor_key.h"

#include <ros/ros.h>
#include <geometry_msgs/QuaternionStamped.h>
#include <geometry_msgs/Vector3Stamped.h>
#include <geometry_msgs/Twist.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/NavSatFix.h>
#include <sensor_msgs/Imu.h>
#include <std_msgs/UInt8.h>
#include <tf/tf.h>
#include <tf/transform_datatypes.h>
#include <sensor_msgs/Joy.h>

// System includes
#include <unistd.h>
#include <iostream>
#include "math.h"
#include <std_msgs/Float32.h>
#include <std_msgs/Int16MultiArray.h>

// pixhawk includes
#include <mavlink/config.h>
#include <mavros_msgs/AttitudeTarget.h>
#include <mavros_msgs/CommandBool.h>
#include <mavros_msgs/CommandTOL.h>
#include <mavros_msgs/State.h>
#include <mavros_msgs/HomePosition.h>
#include <mavros_msgs/RCIn.h>
#include <mavros_msgs/RCOut.h>
#include <mavros_msgs/SetMode.h>
#include <mavros_msgs/CommandHome.h>
#include <mavros_msgs/ParamGet.h>
#include <mavros_msgs/ParamSet.h>
#include <mavros_msgs/ParamPull.h>
#include <mavros_msgs/ParamPush.h>
#include <mavros_msgs/PositionTarget.h>


#include <mavros_msgs/Waypoint.h>
#include <mavros_msgs/WaypointList.h>
#include <mavros_msgs/WaypointClear.h>
#include <mavros_msgs/WaypointPull.h>
#include <mavros_msgs/WaypointPush.h>
#include <mavros_msgs/WaypointReached.h>



#define NUM 1
#define VEL 0.5

#define DEG 0.1
#define ROT 0.05
class pix_control
{
private:
  ros::ServiceClient client_cmdOffBoard;
  ros::ServiceClient client_cmdArming;
  ros::ServiceClient client_cmdTakeoff;
  ros::ServiceClient client_cmdLand;
  ros::ServiceClient client_cmdSetHome;


  ros::ServiceClient client_cmdParam;

  ros::NodeHandle nh;
  ros::Publisher pub_cmdCurrentRPY;
  ros::Publisher pub_cmdAttitude;
  ros::Publisher pub_cmdGPS;
  ros::Publisher pub_cmdRCin;
  ros::Publisher pub_cmdWp;




  ros::Subscriber sub_pix_IMU;
  ros::Subscriber sub_pix_current_target_attitude;
  ros::Subscriber sub_pix_state;

  ros::Subscriber sub_pix_poseGloabl;
  ros::Subscriber sub_pix_poseLocal;

  ros::Subscriber sub_pix_homePosition;
  ros::Subscriber sub_pix_rc;
  ros::Subscriber sub_pix_rc_out;


  ////////////////////////////////////////////
  ///////////////////////////////////////////

  ros::Publisher pub_cmdVel;
  ros::Subscriber sub_pix_target_local;
  nav_msgs::Odometry pix_set;
  mavros_msgs::PositionTarget pix_desired;
  mavros_msgs::PositionTarget pix_raw;
  nav_msgs::Odometry pix_poseLocal;
  char pix_flag;
  /// ////////////////////////////////////////

  ///////////////////////////////////////////

  mavros_msgs::Waypoint pix_way;


  mavros_msgs::ParamGet pix_param;

  double yawInRad; // cur. yaw [rad]
  bool flagOffboard;
  mavros_msgs::State pix_state;
  mavros_msgs::State pix_state_old;

  sensor_msgs::Imu pix_imu;
  mavros_msgs::AttitudeTarget pix_targetAttitude;
  sensor_msgs::NavSatFix pix_poseGlobal;

  mavros_msgs::HomePosition pix_homePosition;
  std_msgs::Int16MultiArray rc_in;
  //rc_out?


  ///////////////////////////////////////////
  geometry_msgs::PoseStamped pix_homeLocal;
  geometry_msgs::PoseStamped pix_targetLocal;
  pix_gtc::WaypointList wp;
  std::vector <geometry_msgs::PoseStamped> pix_wpLocal;


  ros::Time last_request;
  mavros_msgs::SetMode offb_set_mode;

  int pauseItr;
  int idxWp;
  int sizeWp;

  tf::Quaternion relativeRotation(geometry_msgs::Vector3 angle);
  geometry_msgs::Vector3 Quat2Angle(geometry_msgs::Quaternion quat);
  tf::Quaternion Angle2Quat(geometry_msgs::Vector3 angle);

  void callback_IMU(const sensor_msgs::Imu msg);
  void callback_targetAttitude(const mavros_msgs::AttitudeTarget msg);
  void callback_state(const mavros_msgs::State::ConstPtr& msg);


  void callback_GPS(const sensor_msgs::NavSatFix::ConstPtr &msg);
  void callback_local(const nav_msgs::Odometry::ConstPtr &msg);


  void callback_homePosition(const mavros_msgs::HomePosition::ConstPtr &msg);
  void callback_rc_in(const mavros_msgs::RCIn msg);
  //rc_out?
  void callback_Target(const mavros_msgs::PositionTarget::ConstPtr &msg);
public:
  pix_control();
  ~pix_control();

  char connectItr;
  char flagState;
  char flagWp;


  bool check_offboard();
  bool check_connected();
  bool check_homePosition();

  void cmd_OffBoard(bool flag);
  void cmd_Arming();
  void cmd_TakeOff();
  void cmd_Land();
  void cmd_SetHome();

  void cmd_Altitude(bool flag);
  void cmd_posx(bool flag);
  void cmd_Rotation(bool flag);

  void cmd_Vel(char flag, char sign);

//  void cmd_Param();

  void GPS2Local(pix_gtc::WaypointList cmd);
  void cmd_wp();

  void spinOnce();
  void temp();
};

#endif // PIX_CONTROL_H
