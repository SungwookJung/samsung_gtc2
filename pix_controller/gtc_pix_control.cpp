#include "ros/ros.h"
#include "src/pix_control.h"
#include <termios.h>
#include <stdlib.h>

void RestoreKeyboardBlocking(struct termios *initial_settings)
{
  tcsetattr(0, TCSANOW, initial_settings);
}

void SetKeyboardNonBlock(struct termios *initial_settings)
{

    struct termios new_settings;
    tcgetattr(0,initial_settings);

    new_settings = *initial_settings;
    new_settings.c_lflag &= ~ICANON;
    new_settings.c_lflag &= ~ECHO;
    new_settings.c_lflag &= ~ISIG;
    new_settings.c_cc[VMIN] = 0;
    new_settings.c_cc[VTIME] = 0;

    tcsetattr(0, TCSANOW, &new_settings);
}

static void Display_Init_Menu()
{
  std::cout<<"----------- < Init menu > ----------"<<std::endl;
  std::cout<<"[h] Home Position Setting"<<std::endl;
  std::cout<<"[t] Takeoff"<<std::endl;
  std::cout<<"[o] OFFBOARD On"<<std::endl;
  std::cout<<"[a] Arming"<<std::endl;
  std::cout<<""<<std::endl;

  std::cout<<"[g] Get GPS Way Point"<<std::endl;
  std::cout<<""<<std::endl;

  std::cout<<"[q] Exit"<<std::endl;
  std::cout<<"----------------------------------------"<<std::endl;
  std::cout<<"Press Key..."<<std::endl;
}

static void Display_WayPoint_Menu()
{
  std::cout<<"----------- < WayPoint menu > ----------"<<std::endl;
  std::cout<<"[g] Go"<<std::endl;
  std::cout<<"[p] Pause"<<std::endl;
  std::cout<<"[s] Stop"<<std::endl;
  std::cout<<""<<std::endl;

  std::cout<<"[q] System Exit"<<std::endl;
  std::cout<<"----------------------------------------"<<std::endl;
  std::cout<<"Press Key..."<<std::endl;
}

static void Display_Main_Menu(void)
{
    std::cout<<"----------- < Main menu > ----------"<<std::endl;
    std::cout<<"[l] Landing"<<std::endl;
    std::cout<<""<<std::endl;

    std::cout<<"[u] Altitude Up"<<std::endl;
    std::cout<<"[d] Altitude Down"<<std::endl;
    std::cout<<"[6] Right rotation"<<std::endl;
    std::cout<<"[4] Left rotation"<<std::endl;
    std::cout<<"[8] X-axis +1m"<<std::endl;
    std::cout<<"[2] X-axis +1m"<<std::endl;
    std::cout<<""<<std::endl;

    std::cout<<"[g] Get GPS Way Point"<<std::endl;
    std::cout<<"[w] Go Way Point"<<std::endl;
    std::cout<<""<<std::endl;

    std::cout<<"[q] Exit"<<std::endl;
    std::cout<<"----------------------------------------"<<std::endl;
    std::cout<<"Press Key..."<<std::endl;
}











int main(int argc, char **argv){

  int tmp32;
  ros::init(argc, argv, "GTC_Pixhawk_control");
  pix_control * pixhawk = new pix_control();
  ros::Rate loop_rate(50);

  struct termios term_settings;
  SetKeyboardNonBlock(&term_settings);

  Display_Init_Menu();
//  Display_Main_Menu();

  mavros_msgs::SetMode offb_set_mode;
  offb_set_mode.request.custom_mode = "OFFBOARD";

  ros::NodeHandle nh;
  ros::ServiceClient monitor_client = nh.serviceClient<pix_gtc::monitor_key>("monitor_srv");
  pix_gtc::monitor_key monitor_srv;
  pix_gtc::WaypointList waypointList;

  while(ros::ok())
  {
    ros::spinOnce();
    pixhawk->spinOnce();
    if(pixhawk->connectItr == 10){
      printf("Pixhawk connet fail, please press [q] and [Enter]\n");
    }

    loop_rate.sleep();
    tmp32 = getchar();


    if(tmp32 > 0)
    {
      std::system("clear");

      if(pixhawk->flagState < 4) Display_Init_Menu();
      else if(pixhawk->flagState == 4) Display_Main_Menu();
      else Display_WayPoint_Menu();


      std::cout<<"input: "<<(char)tmp32<<std::endl;
      std::cout<<"Run? then, press Enter. If not, press any other key"<<std::endl;
      int cmdInput = 0;

      while(!(cmdInput>0))
      {
        ros::spinOnce();
        pixhawk->spinOnce();
        loop_rate.sleep();
        cmdInput = getchar();
      }

      if(cmdInput != 10)
      {
        std::system("clear");
        if(pixhawk->flagState < 4) Display_Init_Menu();
        else if(pixhawk->flagState == 4) Display_Main_Menu();
        else Display_WayPoint_Menu();
        continue;
      }

      switch (tmp32)
      {
      case 'h': // home position setting at cur. gps
        pixhawk->cmd_SetHome();
//        if(pixhawk->flagState == 0) pixhawk->cmd_SetHome();
        break;
      case 'o': // offboard on
        pixhawk->cmd_OffBoard(true);
//        if(pixhawk->flagState == 2) pixhawk->cmd_OffBoard(true);
//        else ROS_ERROR("You need to perform prior command");
        break;
      case 'a': // obtain cont.
        pixhawk->cmd_Arming();
//        if(pixhawk->flagState == 3) pixhawk->cmd_Arming();
//        else ROS_ERROR("You need to perform prior command");
        break;
      case 't': // takeoff
        ros::spinOnce();
        pixhawk->cmd_Vel(4, 0);
//        pixhawk->cmd_TakeOff();
//        if(pixhawk->flagState == 1) pixhawk->cmd_TakeOff();
//        else ROS_ERROR("You need to perform prior command");
        break;

      case 'u': // altitude up, after take off
        ros::spinOnce();
        if(pixhawk->flagState == 4) pixhawk->cmd_Vel(4, 1);
        else ROS_ERROR("Drone is not in the sky");
        break;
      case 'd': // altitude down, after take off
        ros::spinOnce();
        if(pixhawk->flagState == 4) pixhawk->cmd_Vel(5, 2);
        else ROS_ERROR("Drone is not in the sky");
        break;
      case '8': // +x-axis
        ros::spinOnce();
        if(pixhawk->flagState == 4) pixhawk->cmd_Vel(0, 3);
        else ROS_ERROR("Drone is not in the sky");
        break;
      case '2': // -x-axis
        ros::spinOnce();
        if(pixhawk->flagState == 4) pixhawk->cmd_Vel(1,4);
        else ROS_ERROR("Drone is not in the sky");
        break;
      case '6': // +y
        ros::spinOnce();
        if(pixhawk->flagState == 4) pixhawk->cmd_Vel(2,5);
        else ROS_ERROR("Drone is not in the sky");
        break;
      case '4': // -y
        ros::spinOnce();
        if(pixhawk->flagState == 4) pixhawk->cmd_Vel(3,6);
        else ROS_ERROR("Drone is not in the sky");
        break;
      case 'l': // landing
        ros::spinOnce();
        if(pixhawk->flagState == 4) pixhawk->cmd_Vel(5, 7);
        else ROS_ERROR("Drone is not in the sky");
        break;


      case '9': // Right rot.
        ros::spinOnce();
        if(pixhawk->flagState == 4) pixhawk->cmd_Vel(6, 8);
        else ROS_ERROR("Drone is not in the sky");
        break;
      case '7': // Left rot.
        ros::spinOnce();
        if(pixhawk->flagState == 4) pixhawk->cmd_Vel(7, 9);
        else ROS_ERROR("Drone is not in the sky");
        break;



      case 'g':
        ros::spinOnce();
        if(pixhawk->flagState < 5){
          monitor_srv.request.key = 'g';
          if(monitor_client.call(monitor_srv)){
            waypointList = monitor_srv.response.wpList;
            for(int i = 0; i < waypointList.WaypointList.size(); i++){
              printf("%lf %lf %ld\n", waypointList.WaypointList[i].longitude, waypointList.WaypointList[i].latitude, waypointList.WaypointList.size());
            }
            pixhawk->GPS2Local(waypointList);
          }
          else{
            ROS_ERROR("fail monitor srv");
            return 1;
          }
        }
        else{
          if((pixhawk->flagState == 5) && (pixhawk->flagWp == 2)){
            pixhawk->flagWp = -1;
            pixhawk->cmd_wp();
          }
          else{
            ROS_ERROR("????");
          }
        }
        break;
      case 'p':
        ros::spinOnce();
        if((pixhawk->flagState == 5) && (pixhawk->flagWp == 0)){
          ROS_INFO("WP mission pause // If you wanna go wp, Using the key 'p'");
          pixhawk->flagWp = 3;
        }
        else if((pixhawk->flagState == 5) && (pixhawk->flagWp == 4)){
          pixhawk->flagWp = 5;
        }
        break;
      case 'w':
        ros::spinOnce();
        if(pixhawk->flagState < 5) pixhawk->cmd_wp();
        else ROS_ERROR("nop");
        break;
      case 's':
        ros::spinOnce();
        if(pixhawk->flagState == 5){
          ROS_INFO("Wp misiion stop, Wp clear");
          pixhawk->flagWp = 6;

          monitor_srv.request.key = 's';
          if(monitor_client.call(monitor_srv)){
            waypointList.WaypointList.clear();
          }
        }
        break;


      case 'q': // sys. exit
        RestoreKeyboardBlocking(&term_settings);
        return 0;
      }

    }
  }
  return 0;
}
